=head
Things to parameterize:
	1) Ways to compute MINUS_INFINITY
              a) as a constant parameter
              b) defined by smallest percentage of word occurrence in a tag
      2) How to compute frequent word list
              a) a pre-defined dictionary
		   b) derived from corpus
      3) How to count words 
              a) Use sentence count
              b) Just once for each unique sentence

=cut
use Storable;
use Data::Dumper;
use strict;
#use warnings;
our  %excludedWords;
require "./lib/wordstemmer.pl";


if (@ARGV != 2) {
   die "Usage: perl testNLU.pl <model file> <testset>";
}
#Declare some global variables
my  %TagCount;
my %TagWordProbVector;
my  %TagWordMissingVector;

my  %WordIndex;
my  $NumUniqueWords;
my %Score;

my %LogTagProbability;

runTest(@ARGV);



#Initialize variables

sub runTest {
  my ($modelfile, $testset) = @_;
  if (! defined $modelfile or ! defined $testset) {
     die "Usage: perl testNLU.pl  <model file> <testset>";
  }
  my $Params = readModel($modelfile);
  open(TESTSET, $testset)  || die "Can't open $testset";
  <TESTSET>;
  #$|=1;

  my $nsentences=0;
  my $ncorrect=0;
$|=1;
  print "Phrase,Real Tag,Real Tag Score, Guessed Tag,Raw Score,2nd Best,Raw Score, Correct\n";
  while (<TESTSET>) {
   s/\s+$//;
   my ($sentence, $realtag) = split(/,/);
   next if !defined $sentence;
   next if $sentence =~ /^\s*$/;

   my $best = getBest($sentence);
   my $correctflag=0;
   $nsentences++;
   if ($realtag eq $best->{tag}) {
    $correctflag=1;
    $ncorrect++;
   } 

  printf "%s,%s,%s,%s,%s,%s,%s,%d\n", $sentence, $realtag, $Score{$realtag},  $best->{'tag'},  $best->{score}, $best->{'nbest'}->[1], 
           $Score{$best->{'nbest'}->[1]}, $correctflag;
  }
  close TESTSET;
  print "\n\n\n";
  print Dumper $Params;
  printf  "Total correct: %d, %6.2f pct of %d sentences\n\n", $ncorrect, 100 * $ncorrect/$nsentences, $nsentences;
  printf STDERR "Total correct: %d, %6.2f pct of %d sentences\n\n", $ncorrect, 100 * $ncorrect/$nsentences, $nsentences;

}

sub readModel {
  my($modelFile) = @_;

  my $model = retrieve $modelFile;
  %TagCount= %{$model->{TagCount}};
  %TagWordProbVector = %{$model->{TagWordProbVector}};
  %TagWordMissingVector = %{$model->{TagWordMissingVector}};
  %excludedWords = %{$model->{excludedWords}};
  %WordIndex = %{$model->{WordIndex}};
  $NumUniqueWords = $model->{NumUniqueWords};
  %LogTagProbability = %{$model->{LogTagProbability}};
  return $model->{Params};
}




sub getBest {
   my ($sentence) = @_;

   my $vector  = {};

   foreach my $word (keys %{getStems($sentence)}) {
     if (defined $WordIndex{$word}) {
       $vector->{$WordIndex{$word}} = 1; 
     }   
   }
   my $bestscore = undef;
   my $besttag;

   foreach my $tag (keys %TagCount)  {
      my $score = computeScore($TagWordProbVector{$tag}, $TagWordMissingVector{$tag}, $vector, $tag);
      $Score{$tag} = $score;
      if (!defined $bestscore or $score >= $bestscore) {
          $bestscore = $score;
          $besttag=$tag;
      }
   }
   my @nbestlist = sort byscore (keys %TagCount);
   return {'tag' => $besttag, 'score' => $bestscore, 'nbest' => \@nbestlist} ;
}


sub byscore {
    $Score{$b} <=> $Score{$a};
}



sub computeScore { 
   my ($WordExistsVector, $WordMissingVector, $sentencevector, $tag) = @_;
   

   my $score = 0;
   for (my $i=0; $i< $NumUniqueWords; $i++){
     if (defined  $sentencevector->{$i}) { #the word in sentence 
         $score = $score + $WordExistsVector->{$i};
     }
     else { #the word is in tag but not in sentence
         $score = $score + $WordMissingVector->{$i};
     }
   }
   $score = $score + $LogTagProbability{$tag};

   return $score;
}





   


     