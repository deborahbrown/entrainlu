=head
Things to parameterize:
	1) Ways to compute MINUS_INFINITY
              a) as a constant parameter
              b) defined by smallest percentage of word occurrence in a tag
      2) How to compute frequent word list
              a) a pre-defined dictionary
		   b) derived from corpus
      3) How to count words 
              a) Use sentence count
              b) Just once for each unique sentence

=cut

use Storable;
use Data::Dumper;
use strict;
#use warnings;  
our %excludedWords; 
require "./lib/wordstemmer.pl";


if (@ARGV < 1 ) {
  die "Usage: perl trainNLU.pl <training corpus>";
}

my %Params;
my $input = shift @ARGV;
my $paramsFile = shift @ARGV;
if (defined $paramsFile) {
   require $paramsFile;
}
else {

  #The following will eventually be parameters
  $Params{'frequency_dict'} = "frequent.csv";
  $Params{'MINUS_INFINITY'} = -1000;
  $Params{'FullSentenceTradeOff'}=1; #A number between 0 and 1 - determines how each unique sentence count; if 0, the full number of occurrences used; if 1, only counted once
  $Params{'outputFile'}='model.out';
}

my $MINUS_INFINITY=$Params{'MINUS_INFINITY'};
my $FullSentenceTradeOff = $Params{'FullSentenceTradeOff'};


#Initialize variables
my $NumUniqueWords=0;
my %WordIndex;

my %TagCount;
my %TagProbability;
my %TagWordProbVector;
my %TagWordMissingVector;
my %WordOccurrences;
my $SentenceCount=0;
my %WordCount;
my %LogTagProbability;
my %Score;
my $NCorrectForIteratiion=0;
my $LastIterationCorrect=0;

readWordFrequencies();

computeWordStats();
calculateTagProb();
tagLikelihoodModel();
saveModel();

exit();




###################  Subroutines ############################################

sub computeWordStats { #tag and word counts
  open(CORPUS, $input);
  <CORPUS>;
  while(<CORPUS>) {
   s/\s+$//;
   my ($sentence, $tag, $count) = split(/,/);
   next if !defined $sentence;
   next if $sentence =~ /^\s*$/;
   
   $TagCount{$tag} = $TagCount{$tag} + (1-$FullSentenceTradeOff) * $count + $FullSentenceTradeOff;

   my %stems = %{getStems($sentence)};
   foreach my $word (keys %stems) {
     if (!defined $WordIndex{$word}) {
        $WordIndex{$word} = $NumUniqueWords;
        $NumUniqueWords++;
     }

     $WordOccurrences{$word, $tag} = $WordOccurrences{$word, $tag} + (1-$FullSentenceTradeOff)*$count + $FullSentenceTradeOff;
     $WordCount{$word} = $WordCount{$word} + (1-$FullSentenceTradeOff)*$count + $FullSentenceTradeOff;
     $SentenceCount = $SentenceCount + (1-$FullSentenceTradeOff)*$count + $FullSentenceTradeOff;

  
   }
   
  }
  close CORPUS;
}

sub tagLikelihoodModel { #Model based on likelihood sentence if tag, given tag
 foreach my $tag (keys %TagCount) {
     foreach my $word (keys %WordIndex) {
     if (!defined $WordOccurrences{$word, $tag} ) {
            $TagWordProbVector{$tag}->{$WordIndex{$word}} = $MINUS_INFINITY;
     } 
     else {
      $TagWordProbVector{$tag}->{$WordIndex{$word}} = log( $WordOccurrences{$word, $tag} / $TagCount{$tag});
     }
     if ( $WordOccurrences{$word, $tag} == $TagCount{$tag} ) {
            $TagWordMissingVector{$tag}->{$WordIndex{$word}} = $MINUS_INFINITY;
     } 
     else {
      $TagWordMissingVector{$tag}->{$WordIndex{$word}} = log(1 - ($WordOccurrences{$word, $tag} / $TagCount{$tag}));
     }
     
    }

 }
}




sub calculateTagProb {
    my $totalcount;
    foreach my $tag (keys %TagCount) {
       $totalcount = $totalcount + $TagCount{$tag};
    }
    foreach my $tag (keys %TagCount) {
       $LogTagProbability{$tag} = log ($TagCount{$tag}/$totalcount);
    }
}



sub saveModel() {
 my $model =
  {
  "NumUniqueWords" => $NumUniqueWords,
  "WordIndex" => \%WordIndex,
  "TagCount" => \%TagCount,
  "TagWordProbVector" => \%TagWordProbVector,
  "TagWordMissingVector" => \%TagWordMissingVector,
  "LogTagProbability" => \%LogTagProbability,
  "excludedWords" => \%excludedWords,
  "Params" => \%Params
  };
  store $model, $Params{'outputFile'};
}

sub readWordFrequencies {
  open(Frequent, $Params{'frequency_dict'}) or die "Can't open $Params{'frequency_dict'}";
  <Frequent>;
  while (<Frequent> ) {
    chomp;
    my($word) = split(/,/, $_, 2);
    $veryFrequent{$word}++;
  }
  close Frequent;
}







     