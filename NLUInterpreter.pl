use HTTP::Daemon;
use HTTP::Response;
use JSON::Parse 'parse_json';
use Storable;
use Data::Dumper;
our  %veryFrequent;
require "./lib/wordstemmer.pl";

my  %Tags;
my %TagWordProbVector;
my  %TagWordMissingVector;

my  %WordIndex;
my  $WordCount;
my %Score;

umask 000;

if (@ARGV < 1 ) {
   die "Usage: perl NLUInterpreter.pl <port>";
}

$port=shift @ARGV;


my $match_TEMPLATE =<<EOTEMPLATE;
{
  "tag" : "%s",
  "grammar" : "%s",
  "match" : "MATCH",
  "slots" : {}
}
EOTEMPLATE

my $nomatch_TEMPLATE =<<EOTEMPLATE;
{
  "grammar" : "%s",
  "match" : "NOMATCH"
}
EOTEMPLATE


$SIG{CHLD} = "IGNORE";


my $d = new HTTP::Daemon
  LocalAddr => '127.0.0.1:5000',
  Reuse=> 1,
  LocalPort => $port;
$|=1;

print "Please contact me at: <URL:", $d->url, ">\n";

 my $Params;
 while (my $c = $d->accept) {

         LogEvent(4, "got request");
=head
       FORK: {
        if (my $pid = fork) {
            next;
        }
=cut
        if (my $r = $c->get_request) {
                
              my $message = $r->decoded_content;
              my $model = sprintf "%s.model", parse_json($message)->{'grammar'};
              my $sentence = parse_json($message)->{'userresp'};
              $Params = readModel($model);
              my $interpretation = getIntent($sentence);
              my $result = sprintf $match_TEMPLATE, $interpretation->{'tag'},$model;
              my $response = HTTP::Response->new(200,"OK", undef, $result);
              $c->send_response($response);
              $c->force_last_request;
              $c->close();
              undef $c;
              exit();
         
           }
  #    }
                  
}

sub LogEvent {
    my $level = shift; # 0 - logging off; 1 - critical ; 2 - warning ; 3 - info ; 4 - debug
    my $event = shift;
    my @info = @_;


    printf STDERR "%s\n", join("\|", $event, @info);

}



#Initialize variables



sub readModel {
  my($modelFile) = @_;

  my $model = retrieve $modelFile;
  %TagCount= %{$model->{TagCount}};
  %TagWordProbVector = %{$model->{TagWordProbVector}};
  %TagWordMissingVector = %{$model->{TagWordMissingVector}};
  %veryFrequent = %{$model->{veryFrequent}};
  %WordIndex = %{$model->{WordIndex}};
  $NumUniqueWords = $model->{NumUniqueWords};
  %LogTagProbability = %{$model->{LogTagProbability}};
  return $model->{Params};
}return $model->{Params};
}



sub getIntent {
   my ($sentence) = @_;

   my $vector  = {};

   foreach my $word (keys %{getStems($sentence)}) {
     if (defined $WordIndex{$word}) {
       $vector->{$WordIndex{$word}} = 1; 
     }   
   }
   my $bestscore = undef;
   my $besttag;

   foreach my $tag (keys %Tags)  {
      my $score = computeScore($TagWordProbVector{$tag}, $TagWordMissingVector{$tag}, $vector, $tag);
      $Score{$tag} = $score;
      if (!defined $bestscore or $score >= $bestscore) {
          $bestscore = $score;
          $besttag=$tag;
      }
   }
   my @nbestlist = sort byscore (keys %Tags);
   return {'tag' => $besttag, 'score' => $bestscore, 'nbest' => \@nbestlist} ;
}


sub byscore {
    $Score{$b} <=> $Score{$a};
}


sub computeScore { 
   my ($WordExistsVector, $WordMissingVector, $sentencevector, $tag) = @_;
   

   my $score = 0;
   for (my $i=0; $i< $NumUniqueWords; $i++){
     if (defined  $sentencevector->{$i}) { #the word in sentence 
         $score = $score + $WordExistsVector->{$i};
        $RecomputedTagCount{$tag} = $RecomputedTagCount{$tag} + exp($WordExistsVector->{$i});
     }
     else { #the word is in tag but not in sentence
         $score = $score + $WordMissingVector->{$i};
        $RecomputedTagCount{$tag} = $RecomputedTagCount{$tag} + exp($WordMissingVector->{$i});
     }
   }
   $score = $score + $LogTagProbability{$tag};


   return $score;
}

